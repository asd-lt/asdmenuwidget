<?php
/**
 * @package ImpressPages
 *
 *
 */

namespace Plugin\AsdMenuWidget\Widget\Menu;
class Helper
{
    public static function getMenuItems($menuId, $depthFrom = 1, $depthTo = 1000, $orderBy = null)
    {
        if ($orderBy == 'title') {
            $order = '`title`';
        } else {
            $order = '`pageOrder`';
        }

        $breadcrumb = ipContent()->getBreadcrumb();

        $languageCode = ipContent()->getCurrentLanguage()->getCode();
        $elements = ipDb()->selectAll(
            'page',
            '*',
            array('isVisible' => 1, 'isSecured' => 0, 'parentId' => $menuId, 'isDeleted' => 0),
            "ORDER BY $order"
        ); 

        $items = array();
        if (!empty($elements)) {
            $items = self::arrayToMenuItem($elements, $depthTo, $depthFrom, $order);
        }

        return $items;
    }

    public static function getChildItems($pageId = null, $depthLimit = 1000, $orderBy = null)
    {
        if ($orderBy == 'title') {
            $order = '`title`';
        } else {
            $order = '`pageOrder`';
        }

        if (is_string($pageId) && !ctype_digit($pageId)) {
            // $pageId is an alias. Get the real id;
            $pageId = ipContent()->getPage($pageId)->getId();
            if (!$pageId) {
                return array();
            }
        }
        if ($pageId === null) {
            $pageId = ipContent()->getCurrentPage()->getId();
        }

        $elements = ipDb()->selectAll(
            'page',
            '*',
            array('isVisible' => 1, 'isSecured' => 0, 'parentId' => $pageId, 'isDeleted' => 0),
            "ORDER BY $order"
        ); //get first level elements

        $items = array();
        if (!empty($elements)) {
            $items = self::arrayToMenuItem($elements, $depthLimit, 1, $order);
        }

        return $items;
    }

    private static function arrayToMenuItem($pages, $depth, $curDepth, $order)
    {
        $items = array();
        foreach ($pages as $pageRow) {
            $page = new \Ip\Page($pageRow['id']);
            $item = new Item();
            $subSelected = false;
            if ($curDepth < $depth) {
                $children = ipDb()->selectAll(
                    'page',
                    '*',
                    array('parentId' => $page->getId(), 'isVisible' => 1, 'isSecured' => 0, 'isDeleted' => 0),
                    "ORDER BY $order"
                );
                if ($children) {
                    $childrenItems = self::arrayToMenuItem($children, $depth, $curDepth + 1, $order);
                    $item->setChildren($childrenItems);
                }
            }
            if ($page->isCurrent() || $page->getRedirectUrl() && $page->getLink(
                ) == \Ip\Internal\UrlHelper::getCurrentUrl()
            ) {
                $item->markAsCurrent(true);
            } elseif ($page->isInCurrentBreadcrumb() || $subSelected || $page->getRedirectUrl(
                ) && self::existInBreadcrumb($page->getLink())
            ) {
                $item->markAsInCurrentBreadcrumb(true);
            }

            if ($page->isDisabled()) {
                $item->setUrl('');
            } elseif ($page->getRedirectUrl()) {
                $url = $page->getRedirectUrl();
                if (!preg_match('/^((http|https):\/\/)/i', $url)) {
                    $url = 'http://' . $url;
                }
                $item->setUrl($url);
            } else {
                $item->setUrl($page->getLink());
            }
            $metaTitle = $page->getMetaTitle();
            $item->setBlank($page->isBlank());
            $item->setTitle($page->getTitle());
            $item->setDepth($curDepth);
            $item->setDisabled($page->isDisabled());
            $item->setId($page->getId());
            $item->setAlias($page->getAlias());
            $item->setPageTitle(empty ($metaTitle) ? $page->getTitle() : $metaTitle);
            $items[] = $item;
        }

        return $items;
    }

    private static function existInBreadcrumb($link)
    {
        $breadcrumb = ipContent()->getBreadcrumb();
        foreach ($breadcrumb as $element) {
            if ($element->getLink() == $link && !$element->getRedirectUrl()) {
                return true;
            }
        }

        return false;
    }

}
