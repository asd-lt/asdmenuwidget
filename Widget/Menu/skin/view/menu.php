<?php if( !empty( $data ) ): ?>
    <ul class="<?php echo $data['menu']['class']; ?>">
        <?php $i = 0; if( !empty( $data['items'] ) ): $items = $data['items']; ?>
            <?php foreach( $items as $item ): $i++; ?>
                <li <?php echo ( $item->isCurrent() || $item->isInCurrentBreadcrumb() ) ? 'class="'.$data['menu']['active'].'"' : null; ?>>
                    <a <?php if( !$item->isDisabled() ) echo 'href="'.$item->getUrl().'"'; ?><?php echo $item->getBlank( ) ? 'target="_blank"' : ''; ?>>
                        <?php if( !empty( $data['icon']['enable'] ) ): ?>
                            <?php 
                                if( ipIsManagementState() ) {
                                    $defaultImage = "http://dummyimage.com/{$data['icon']['width']}x{$data['icon']['height']}/f5f5f5/888";
                                } elseif( !empty( $data['icon']['empty'] ) ) {
                                    $defaultImage = ipFileUrl( 'Plugin/AsdMenuWidget/Widget/Menu/assets/empty.png' );
                                } else {
                                    $defaultImage = '';
                                }
                                $title = $item->getTitle();
                                $image = ipSlot( 'image', array( 'id' => "asd-menu-item-image-{$data['menu']['name']}-{$item->getId()}", 'pageId' => $data['menu']['pageId'], 'height' => $data['icon']['height'], 'width' => $data['icon']['width'], 'default' => $defaultImage, 'alt' => $title, 'title' => $title ) );
                            ?>
                            <?php if( !empty( $image ) ): ?>
                                <span class="icon-place"><?php echo $image; ?></span>
                            <?php endif; ?>
                            <?php if( !empty( $data['icon']['text'] ) ): ?>
                                <?php echo $title; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo $item->getTitle(); ?>
                        <?php endif; ?>
                    </a>
                    <?php if( empty( $data['menu']['nochild'] ) || ( !empty( $data['menu']['nochild'] ) && ( $item->isCurrent() || $item->isInCurrentBreadcrumb() ) ) ): ?>
                        <?php $data['menu']['class'] = $data['menu']['parent']; $data['items'] = $item->getChildren(); if( !empty( $data['items'] ) && ( empty( $data['menu']['depth'] ) || $depth < $data['menu']['depth'] ) ): ?>
                            <?php echo ipView( 'menu.php', array( 'data' => $data, 'depth' => $depth+1 ) )->render(); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
<?php endif; ?>