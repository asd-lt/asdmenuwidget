<?php
namespace Plugin\AsdMenuWidget;
 
class Slot {
    public static function image( $params )
    {
        $options = array();
        $defaultValue = '';
        $cssClass = '';
        if (empty($params['id'])) {
            throw new \Ip\Exception("Ip.image slot requires parameter 'id'");
        }
        $key = $params['id'];

        if (isset($params['default'])) {
            $defaultValue = $params['default'];
        }

        if (isset($params['width'])) {
            $options['width'] = $params['width'];
        }
        if (isset($params['height'])) {
            $options['height'] = $params['height'];
        }
        
        if (isset($params['pageId'])) {
            $options['pageId'] = $params['pageId'];
        }
        
        if (isset($params['class'])) {
            $cssClass = $params['class'];
        }
        
        if (isset($params['alt'])) {
            $options['attributes']['alt'] = $params['alt'];
        }
        
        if (isset($params['title'])) {
            $options['attributes']['title'] = $params['title'];
        }

        $inlineManagementService = new \Ip\Internal\InlineManagement\Service();
        return $inlineManagementService->generateManagedImage($key, $defaultValue, $options, $cssClass);
    }

    public static function asd_menu( $params ) {
        $data = array(
            'menu' => array(
                'class' => 'menu',
                'parent' => 'parent',
                'active' => 'active',
                'depth' => 0,
                'hidechild' => 0,
            ),
            'icon' => array(
                'width' => 50,    
                'height' => 50,
                'text' => false,
                'empty' => false,
                'enable' => false
            )
        );

        if( !empty( $params['name'] ) ) {
            $page = \Ip\Internal\Pages\Model::getPageByAlias( ipContent()->getCurrentLanguage()->getCode(), $params['name'] );
            $data['menu']['name'] = $page['id'];
        } elseif( !empty( $params['id'] ) ) {
            $data['menu']['name'] = $params['id'];
        }

        if( !empty( $params['class'] ) ) {
            $data['menu']['class'] = $params['class'];
        }

        if( !empty( $params['active'] ) ) {
            $data['menu']['active'] = $params['active'];
        }

        if( !empty( $params['parent'] ) ) {
            $data['menu']['parent'] = $params['parent'];
        }

        if( !empty( $params['depth'] ) ) {
            $data['menu']['depth'] = $params['depth'];
        }

        if( !empty( $params['nochild'] ) ) {
            $data['menu']['nochild'] = $params['nochild'];
        }

        if( !empty( $params['icon'] ) ) {
            $data['icon']['enable'] = true;
            if( !empty( $params['icon']['width'] ) ) {
                $data['icon']['width'] = $params['icon']['width'];
            }
            if( !empty( $params['icon']['height'] ) ) {
                $data['icon']['height'] = $params['icon']['height'];
            }
            if( !empty( $params['icon']['text'] ) ) {
                $data['icon']['text'] = $params['icon']['text'];
            }
            if( !empty( $params['icon']['empty'] ) ) {
                $data['icon']['empty'] = $params['icon']['empty'];
            }
        }

        echo ipView('Widget/Menu/skin/default.php', array( 'data' => $data ))->render();

    }
}
